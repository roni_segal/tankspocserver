package com.tanks.server.desktop;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;
import java.util.Vector;

import javax.xml.crypto.Data;

import sun.applet.resources.MsgAppletViewer_ja;

import static java.lang.System.exit;

/**
 * Created by ronio on 3/3/2017.
 */
public class ClientHandler {
    enum State {notLogedin, inLobby, waitToConfirmBattle, confirmBattle, readyToPlay}; // abortBattle
    public static final int V_WIDTH = 800;
    public static final int V_HEIGHT = 600;
    public static final char SPLITER = '|';
    public static final char END_MESSAGE = '$';
    public static final String REGISTER = "register";
    public static final String LOGIN = "login";
    public static final String CONNECTED_PLAYERS = "CONNECTEDLIST";
    public static final String ASK_FOR_BATTLE = "askforbattle";
    public static final String CONFIRM_BATTLE = "confirmbattle";
    public static final String ABORT_BATTLE = "abortbattle";
    public static final String READY_TO_PLAY = "readytoplay";
    public static final String CREATE_TANK = "createtank";
    public static final String LEFT_MOUSE_CLICK = "leftmouseclick";
    public static final String RIGHT_MOUSE_CLICK = "rightmouseclick";
    public static final String GET_MONEY = "getmoney";
    public static final String FILL_SHOP = "fillshop";
    public static final String SHOP_BODY = "shopbody";
    public static final String SHOP_BARRLE = "shopbarrle";
    public static final String BUY_BARRLE = "buybarrle";
    public static final String BUY_BODY = "buybody";
    public static final String UPDATE_MY_BODYS = "updatemybodys";
    public static final String UPDATE_MY_BARRLES = "updatemybarrles";
    public static final String   SET_MONEY = "setmoney";

    //enum ProjectileType {StraightProjectile, BowProjectile}

    private Socket clientSocket;
    private BufferedReader in;
    private DataOutputStream out;

    private Thread loopThread;
    private boolean clientDisconnected;

    public int playerID;
    public String username;
    public State playerState;
    public ClientHandler conversationWith;


    public ClientHandler(Socket clientSocket) {
        clientDisconnected = false;
        conversationWith = null;
        username = "";
        this.clientSocket = clientSocket;
        playerState = State.notLogedin;
        playerID = -1;
        try {
            out = new DataOutputStream(clientSocket.getOutputStream());
            in = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream()));
        } catch (IOException e) {
            System.err.println("Could not get out stream or in stream for the client");
            System.exit(1);
        }

        loopThread = new Thread(this::loop);
        loopThread.start();
    }

    private void loop() {
        String ret;
        while(true) {
            String[] msg = Read();
            if(msg != null) {
                switch (msg[0]) {
                    // region REGISTER
                    case REGISTER:
                        Write(REGISTER, "" + Register(msg[1], msg[2]));
                        playerState = State.inLobby;
                        break;
                    // endregion
                    // region LOGIN
                    case LOGIN:
                        Write(LOGIN, "" + Login(msg[1], msg[2]));
                        playerState = State.inLobby;
                        break;
                    // endregion
                    // region CONNECTED_PLAYERS
                    case CONNECTED_PLAYERS:
                        ret = "" + CONNECTED_PLAYERS;
                        ret += "" + SPLITER;
                        for(int i = 0; i < Sever.clientHandlers.size(); i++) {
                            ret += Sever.clientHandlers.elementAt(i).playerID;
                            ret += SPLITER;
                            ret += Sever.clientHandlers.elementAt(i).username;
                            ret += SPLITER;
                        }
                        Write(ret);
                        break;
                    // endregion
                    // region ASK_FOR_BATTLE
                    case ASK_FOR_BATTLE:
                        int playerToAsk = Integer.parseInt(msg[1]);
                        for(int i = 0; i < Sever.clientHandlers.size(); i++) {
                            if(Sever.clientHandlers.elementAt(i).playerID == playerToAsk) {
                                conversationWith = Sever.clientHandlers.elementAt(i);
                                Sever.clientHandlers.elementAt(i).conversationWith = this;
                                Sever.clientHandlers.elementAt(i).playerState = State.waitToConfirmBattle;
                                playerState = State.waitToConfirmBattle;
                                Sever.clientHandlers.elementAt(i).Write(ASK_FOR_BATTLE, "" + playerID + SPLITER + username);
                                Write(ASK_FOR_BATTLE, "" + playerToAsk + SPLITER + Sever.clientHandlers.elementAt(i).username);
                                break;
                            }
                        }
                        break;
                    // endregion
                    // region ABORT_BATTLE
                    case ABORT_BATTLE:
                        conversationWith.Write(ABORT_BATTLE, "" + playerID);
                        Write(ABORT_BATTLE, "" + conversationWith.playerID);
                        conversationWith.playerState = State.inLobby;
                        playerState = State.inLobby;
                        conversationWith.conversationWith = null;
                        conversationWith =  null;
                        break;
                    // endregion
                    // region CONFIRM_BATTLE
                    case CONFIRM_BATTLE:
                        playerState = State.confirmBattle;
                        if(conversationWith.playerState == State.confirmBattle) {
                            conversationWith.Write(CONFIRM_BATTLE, playerID);
                            Write(CONFIRM_BATTLE, conversationWith.playerID);
                        }
                        break;
                    // endregion
                    // region READY_TO_PLAY
                    case READY_TO_PLAY:
                        playerState = State.readyToPlay;
                        if(conversationWith.playerState == State.readyToPlay) {
                            conversationWith.Write(READY_TO_PLAY, playerID);
                            Write(READY_TO_PLAY, conversationWith.playerID);

                            InitBattleField();
                        }
                        break;
                    // endregion
                    // region LEFT_MOUSE_CLICK
                    case LEFT_MOUSE_CLICK:
                        Write(LEFT_MOUSE_CLICK, playerID, msg[1], msg[2], msg[3]);
                        conversationWith.Write(LEFT_MOUSE_CLICK, playerID, msg[1], msg[2], V_HEIGHT - Float.parseFloat(msg[3]));
                        break;
                    // endregion
                    // region RIGHT_MOUSE_CLICK
                    case RIGHT_MOUSE_CLICK:
                        Write(RIGHT_MOUSE_CLICK, playerID, msg[1], msg[2], msg[3]);
                        conversationWith.Write(RIGHT_MOUSE_CLICK, playerID, msg[1], msg[2], V_HEIGHT - Float.parseFloat(msg[3]));
                        break;
                    // endregion
                    // region GET_MONEY
                    case GET_MONEY:
                        Write(GET_MONEY, DatabaseHendler.getMoney(playerID));
                        break;
                    // endregion
                    // region FILL_SHOP
                    case FILL_SHOP:
                        Vector<String> bodys = DatabaseHendler.getTankBodyShop();
                        for (String body : bodys) {
                            Write(SHOP_BODY, body);
                        }
                        Vector<String> barrles = DatabaseHendler.getTankBarrleShop();
                        for(String barrle : barrles) {
                            Write(SHOP_BARRLE, barrle);
                        }
                        break;
                    // endregion
                    //region BUY_BODY
                    case BUY_BODY:
                        DatabaseHendler.buyDody("" + playerID, msg[1]);
                        break;
                    // endregion
                    //region BUY_BARRLE
                    case BUY_BARRLE:
                        DatabaseHendler.buyBarrle("" + playerID, msg[1]);
                        break;
                    //endregion
                    //region SET_MONEY
                    case SET_MONEY:
                        DatabaseHendler.setMoney("" + playerID, msg[1]);
                        break;
                    //endregion
                    //region UPDATE_MY_BARRLES
                    case UPDATE_MY_BARRLES:
                        updateMyBarrles();
                        break;
                    //endregion
                    //region UPDATE_MY_BODYS
                    case UPDATE_MY_BODYS:
                        updateMyBodys();
                        break;
                    //endregion
                }
            }
            else {
                break;
            }
        }
        System.out.println("client disconnect");
        try {
            clientSocket.close();
            clientDisconnected = true;
            Sever.clientHandlers.remove(this);
        } catch (IOException e) {
            System.err.println("Could not close socket with the client");
        }
    }

    private String updateMyBodys() {
        Vector<String> bodyIDs = DatabaseHendler.getMyBodys("" + playerID);
        String ret = UPDATE_MY_BODYS + SPLITER;
        int i = 0;
        for (; i < bodyIDs.size() - 1; i++) {
            ret += bodyIDs.elementAt(i) + SPLITER;
        }
        ret += bodyIDs.elementAt(i + 1);
        return ret;
    }

    private String updateMyBarrles() {
        Vector<String> barrleIDs = DatabaseHendler.getMyBarrles("" + playerID);
        String ret = UPDATE_MY_BARRLES + SPLITER;
        int i = 0;
        for (; i < barrleIDs.size() - 1; i++) {
            ret += barrleIDs.elementAt(i) + SPLITER;
        }
        ret += barrleIDs.elementAt(i + 1);
        return ret;
    }


    private void InitBattleField() {
        int numOfPlayer1 = 5;
        int numOfPlayer2 = 4;

        String msg = "";

        for(int i = 0; i < numOfPlayer1; i++) {
            msg = CreateTank(playerID,
                    (V_WIDTH / numOfPlayer1) * 0.5f + (V_WIDTH / numOfPlayer1) * i,
                    100,
                    "brightbrownbase_1_0.png",
                    "brightbrownturret_1_0.png",
                    "StraightProjectile",
                    1000f,
                    100f,
                    15f,
                    450f,
                    150f,
                    10f);
            Write(msg);
        }

        for(int i = 0; i < numOfPlayer2; i++) {
            msg = CreateTank(conversationWith.playerID,
                    (V_WIDTH / numOfPlayer2) * 0.5f + (V_WIDTH / numOfPlayer2) * i,
                    V_HEIGHT - 100,
                    "lightbrownbase_1_0.png",
                    "lightbrownturret_1_0.png",
                    "StraightProjectile",
                    1000f,
                    100f,
                    15f,
                    450f,
                    150f,
                    10f);
            Write(msg);
        }

        for(int i = 0; i < numOfPlayer1; i++) {
            msg = CreateTank(conversationWith.playerID,
                    (V_WIDTH / numOfPlayer2) * 0.5f + (V_WIDTH / numOfPlayer2) * i,
                    100,
                    "lightbrownbase_1_0.png",
                    "lightbrownturret_1_0.png",
                    "StraightProjectile",
                    1000f,
                    100f,
                    15f,
                    450f,
                    150f,
                    10f);
            conversationWith.Write(msg);

            msg = CreateTank(
                    playerID,
                    (V_WIDTH / numOfPlayer1) * 0.5f + (V_WIDTH / numOfPlayer1) * i,
                    V_HEIGHT - 100,
                    "brightbrownbase_1_0.png",
                    "brightbrownturret_1_0.png",
                    "StraightProjectile",
                    1000f,
                    100f,
                    15f,
                    450f,
                    150f,
                    10f);
            conversationWith.Write(msg);
        }
    }

    /**
     * login the user
     * @param username
     * @param password
     * @return if success return player id, if failed return -1
     */
    private int Login(String username, String password) {
        this.username = username;
        return playerID = DatabaseHendler.Login(username, password);
    }

    /**
     * register a user
     * @param username
     * @param password
     * @return if success return player id, if failed return -1
     */
    private int Register(String username, String password) {
        this.username = username;
        return playerID = DatabaseHendler.Register(username, password);
    }

    private String CreateTank(int playerIDToSend,
                              float x,
                              float y,
                              String baseTexture,
                              String barrelTexture,
                              String projectileType,
                              float totalHP,
                              float rotation_speed,
                              float movement_speed,
                              float shoting_max_range,
                              float shoting_min_range,
                              float reload_time) {
        String ret =
                CREATE_TANK + SPLITER +
                playerIDToSend + SPLITER +
                "" + x + SPLITER +
                "" + y + SPLITER +
                baseTexture + SPLITER +
                barrelTexture + SPLITER +
                projectileType + SPLITER +
                "" + totalHP + SPLITER +
                "" + rotation_speed + SPLITER +
                "" + movement_speed + SPLITER +
                "" + shoting_max_range + SPLITER +
                "" + shoting_min_range + SPLITER +
                "" + reload_time + SPLITER;
        return ret;
    }


    private String[] Read() {
        try {
            //String res = in.readLine();
            String res = "";
            char c;
            do {
                c = (char)in.read();
                res += c;
            } while(c != END_MESSAGE && c != -1);

            System.out.println("Read:" + res);
            res = res.substring(0, res.length() - 1); // remove the end char
            res = res.trim();
            return res.split("\\|"); // TODO: change it to use SPLITER
        } catch (IOException e) {
            System.err.println("Could not read from the client");
            //exit(1);
            clientDisconnected = true;
            return null;
        }
    }
    private void Write(Object... args) {
        String toSend = "";
        for(int i = 0; i < args.length - 1; i++)
            toSend += args[i].toString() + SPLITER;
        toSend += args[args.length - 1];
        System.out.println("Write: " + toSend);
        try {
            out.writeBytes(toSend + END_MESSAGE);
            out.flush();
        } catch (IOException e) {
            System.err.println("Could not send message to the client");
        }
    }
}
