package com.tanks.server.desktop;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Vector;

public class Sever {
	public static Vector<ClientHandler> clientHandlers;
	public static ServerSocket serverSocket;

	public static void main (String[] arg){
		serverSocket = null;
		clientHandlers = new Vector<>();
		DatabaseHendler.Init();
		try {
			serverSocket = new ServerSocket(4444);
		} catch (IOException e) {
			System.err.println("Could not listen on port: 4444.");
			System.exit(1);
		}

		Socket clientSocket = null;
		System.out.println("server running");
		while(true) {
			try {
				clientSocket = serverSocket.accept();
				System.out.println("client connected");
				clientHandlers.add(new ClientHandler(clientSocket));
			} catch (IOException e) {
				System.err.println("Accept failed.");
				System.exit(1);
			}
		}
	}
}
