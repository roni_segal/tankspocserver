package com.tanks.server.desktop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 * Created by ronio on 3/31/2017.
 */

public final class DatabaseHendler {
    private DatabaseHendler() {
        // This will never be called
    }

    private static Connection connection;
    private static Statement statement;

    public static void Init() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:database.db");
            statement = connection.createStatement();
            ExecuteUpdate("PRAGMA foreign_keys = ON;");
        } catch ( Exception e ) {
            System.err.println("error DatabaseHendler:Init " + e.getClass().getName() + ": " + e.getMessage() );
            System.exit(1);
        }
    }

    private static ResultSet ExecuteQuery(String query) {
        System.out.println("ExecuteQuery: " + query);
        try {
            return statement.executeQuery(query);
        } catch (SQLException e) {
            System.err.println("error DatabaseHendler:ExecuteQuery " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            return null;
        }
    }
    private static int ExecuteUpdate(String query) {
        System.out.println("ExecuteUpdate: " + query);
        try {
            return statement.executeUpdate(query);
        } catch (SQLException e) {
            System.err.println("error DatabaseHendler:ExecuteUpdate " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            return 0;
        }
    }

    /**
     * register a user
     * @param username
     * @param password
     * @return true on success, false on failure
     */
    public static int Register(String username, String password) {
        if(ExecuteUpdate("INSERT INTO Users (username, password) VALUES ('V1','V2');".replace("V1", username).replace("V2", password)) == 0) {
            return 0;
        }
        try {
            return ExecuteQuery("select last_insert_rowid()").getInt(1);
        } catch (SQLException e) {
            System.err.println("error DatabaseHendler:Register " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
            return 0;
        }
    }

    public static int Login(String username, String password) {
        ResultSet rs = ExecuteQuery("SELECT ID FROM Users WHERE username = 'V1' AND password = 'V2'".replace("V1", username).replace("V2", password));
        try {
            if(rs.next())
                return rs.getInt("ID");
        } catch (SQLException e) {
            System.err.println("error DatabaseHendler:Login " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
        }
        return -1;
    }

    public static void Close() {
        try {
            statement.close();
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            System.err.println("error DatabaseHendler:Close " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
        }
    }

    public static String getMoney(int playerID) {
        ResultSet rs = ExecuteQuery("SELECT MONEY FROM Users WHERE ID = '" + playerID + "';");
        try {
            if(rs.next())
                return rs.getString("MONEY");
        } catch (SQLException e) {
            System.err.println("error DatabaseHendler:getMoney " + e.getClass().getName() + ": " + e.getMessage());
            System.exit(1);
        }
        return "-1";
    }

    public  static Vector<String> getTankBodyShop() {
        ResultSet bodys = ExecuteQuery("SELECT * FROM TankBodyShop");
        Vector<String> ret = new Vector<>();
        String body = "";
        try {
            while(bodys.next()) {
                body += bodys.getString("ID") + "&";
                body += bodys.getString("ImgPath") + "&";
                body += bodys.getFloat("HP") + "&";
                body += bodys.getFloat("MoveingSpeed") + "&";
                body += bodys.getFloat("RotationSpeed") + "&";
                body += bodys.getInt("TankType") + "&";
                body += bodys.getFloat("COST") + "&";
                ret.add(body);
                body = "";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static Vector<String> getTankBarrleShop() {
        ResultSet barrels = ExecuteQuery("SELECT * FROM TankBarrelShop");
        Vector<String> ret = new Vector<>();
        String barrel = "";
        try {
            while(barrels.next()) {
                barrel += barrels.getString("ID") + "&";
                barrel += barrels.getString("ImgPath") + "&";
                barrel += barrels.getFloat("DMG") + "&";
                barrel += barrels.getFloat("MaxRange") + "&";
                barrel += barrels.getFloat("MinRange") + "&";
                barrel += barrels.getInt("TankType") + "&";
                barrel += barrels.getFloat("COST") + "&";
                barrel += barrels.getFloat("ExplosionRadius") + "&";
                barrel += barrels.getFloat("ReloadTime") + "&";
                ret.add(barrel);
                barrel = "";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static void buyDody(String PlayerID, String bodyID) {
        ExecuteUpdate("INSERT INTO BodyTanks (PlayerID, BodyID) VALUES (V1, V2);".replace("V1", PlayerID).replace("V2", bodyID));
    }

    public static void buyBarrle(String PlayerID, String barrleID) {
        ExecuteUpdate("INSERT INTO BarrleTanks (PlayerID, BarrleID) VALUES (V1, V2);".replace("V1", PlayerID).replace("V2", barrleID));
    }

    public static void setMoney(String playerID, String money) {
        ExecuteUpdate("UPDATE Users SET money=V1 WHERE ID=V2".replace("V1", money).replace("V2", playerID));
    }

    public static Vector<String> getMyBodys(String playerID) {
        ResultSet rs = ExecuteQuery("SELECT BodyID FROM BodysTanks where PlayerID =  " + playerID + ";");
        Vector<String> ret = new Vector<>();
        try {
            while(rs.next()) {
                ret.add("" + rs.getInt("BodyID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }
    public static Vector<String> getMyBarrles(String playerID) {
        ResultSet rs = ExecuteQuery("SELECT BarrleID FROM BarrleTanks where PlayerID =  " + playerID + ";");
        Vector<String> ret = new Vector<>();
        try {
            while(rs.next()) {
                ret.add("" + rs.getInt("BodyID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
